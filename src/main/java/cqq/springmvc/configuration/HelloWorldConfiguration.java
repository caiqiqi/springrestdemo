package cqq.springmvc.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "cqq.springmvc")
public class HelloWorldConfiguration {

}
// 在这里，这个类是主要提供组件，扫描和注释支持。需要注意的是，我们没有任何视图解析器配置，因为我们在Rest案例并不需要。
