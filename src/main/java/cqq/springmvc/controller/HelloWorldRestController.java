package cqq.springmvc.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cqq.springmvc.domain.Message;

@RestController                                                // @RestController 注解，这标志着这个类作为控制器，每一个方法返回域对象/pojo代替一个视图。
public class HelloWorldRestController {

	@RequestMapping("/hello/{player}")                     // @RequestMapping表示方法将被绑定到url：/hello/{player}
	public Message message(@PathVariable String player) {  // @PathVariable表示参数将被绑定到变量 URI 模板

		Message msg = new Message(player, "Hello " + player);
		return msg;
	}

}
